import {CREATE_SHORT_URL_REQUEST, CREATE_SHORT_URL_SUCCESS} from "./actions";

const initialState = {
  response: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SHORT_URL_REQUEST:
      return {...state, response: null};
    case CREATE_SHORT_URL_SUCCESS:
      return {...state, response: action.response};
    default:
      return state;
  }
};

export default reducer;