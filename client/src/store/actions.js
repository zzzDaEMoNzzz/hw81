import axios from 'axios';

export const CREATE_SHORT_URL_REQUEST = 'CREATE_SHORT_URL_REQUEST';
export const CREATE_SHORT_URL_SUCCESS = 'CREATE_SHORT_URL_SUCCESS';

export const createShortUrlRequest = () => ({type: CREATE_SHORT_URL_REQUEST});
export const createShortUrlSuccess = response => ({type: CREATE_SHORT_URL_SUCCESS, response});

export const createShortUrl = originalUrl => {
  return dispatch => {
    dispatch(createShortUrlRequest());

    axios.post('/', {URL: originalUrl}).then(
      response => dispatch(createShortUrlSuccess(response.data))
    );
  }
};