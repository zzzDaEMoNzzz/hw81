import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import ThunkMiddleware from 'redux-thunk';
import reducer from "./store/reducer";
import {apiURL} from "./constants";
import './index.css';
import App from './App';

import axios from 'axios';
axios.defaults.baseURL = apiURL;

const store = createStore(reducer, applyMiddleware(ThunkMiddleware));

const app = (
  <Provider store={store}>
    <App/>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));