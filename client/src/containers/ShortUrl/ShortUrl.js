import React, {Component, Fragment} from 'react';
import {apiURL} from "../../constants";
import {connect} from "react-redux";
import './ShortUrl.css';
import {createShortUrl} from "../../store/actions";

class ShortUrl extends Component {
  state = {
    url: '',
  };

  onUrlChange = event => this.setState({url: event.target.value});

  onFormSubmit = event => {
    event.preventDefault();

    this.props.createShortUrl(this.state.url);
  };

  render() {
    let responseMessage = null;

    if (this.props.response) {
      const url = `${apiURL}/${this.props.response.shortUrl}`;

      responseMessage = (
        <Fragment>
          <p>Your link now looks like this:</p>
          <p><a href={url}>{url}</a></p>
        </Fragment>
      );
    }

    return (
      <div className="ShortUrl">
        <form onSubmit={this.onFormSubmit}>
          <h1>Shorten your link!</h1>
          <input
            type="text"
            placeholder="Enter URL here"
            value={this.state.url}
            onChange={this.onUrlChange}
          />
          <button>Shorten!</button>
        </form>
        {responseMessage}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  response: state.response
});

const mapDispatchToProps = dispatch => ({
  createShortUrl: url => dispatch(createShortUrl(url))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShortUrl);