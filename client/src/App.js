import React, { Component } from 'react';
import './App.css';
import ShortUrl from "./containers/ShortUrl/ShortUrl";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ShortUrl/>
      </div>
    );
  }
}

export default App;
