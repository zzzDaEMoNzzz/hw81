const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const nanoid = require('nanoid');
const Link = require('./models/Link');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect('mongodb://localhost/links', {useNewUrlParser: true}).then(() => {
  app.get('/:shortUrl', (req, res) => {
    Link.findOne({shortUrl: req.params.shortUrl})
      .then(link => {
        if (link) res.status(301).redirect(link.originalUrl);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  app.post('/', (req, res) => {
    if(req.body.URL) {
      const link = new Link({
        originalUrl: req.body.URL,
        shortUrl: nanoid(7)
      });

      link.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    } else res.sendStatus(400);
  });

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});