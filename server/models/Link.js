const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  originalUrl: {
    type: String,
    required: true
  },
  shortUrl: {
    type: String,
    required: true
  }
});

const Link = mongoose.model('Link', ProductSchema);

module.exports = Link;